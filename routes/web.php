<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('read', 'ReadController@index');
Route::post('insert', 'InsertController@index');
Route::post('delete', 'DeleteController@index');

Route::post('getdetails', 'GetDetailsController@index');

Route::post('edit', 'EditController@index');