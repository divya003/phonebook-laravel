<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EditController extends Controller
{

    function index(Request $request)
    {
        $data=array();
        $id=$request->get('id');
        $name=$request->get('name');
        $phone=$request->get('phone');
        $illegal = "#$%^&*()+=-[]';,./{}|:<>?~";
        
        //duplicate check
       // $querycheck="SELECT * FROM details WHERE phone='$phone'";
        //$rowcount=DB::select($querycheck);
    
        
        
        $query="UPDATE details SET name='$name' ,phone='$phone' WHERE id='$id'";
        
        
        if(preg_match('/[0-9]/',$name))
            $data+=array("status"=>"failure","message"=>"Names don't look nice with numbers");
        else if(strpbrk($phone,$illegal)===true)
            $data+=array("status"=>"failure","message"=>"Names with special characters can't be called");
        else if(strpbrk($phone,$illegal)===true)
            $data+=array("status"=>"failure","message"=>"Phone Names with special characters can't be called");
        else if(preg_match('/[A-Za-z]/',$phone))
            $data+=array("status"=>"failure","message"=>"Phone numbers don't with alphabets don't work");
        else if($name==null||ctype_space($name))
            $data+=array("status"=>"failure","message"=>"Names that can be called contain some alphabets");
        else if($phone==null||ctype_space($phone))
            $data+=array("status"=>"failure","message"=>"Phone numbers that can be called contain some numbers");
        else if(strlen($phone)!=10)
            $data+=array("status"=>"failure","message"=>"10 numbers make a dialable phone number");
    //   else if(empty($rowcount)==false)
      //      $data+=array("status"=>"failure","message"=>"Duplicate phone numbers not allowed");
        else if(DB::update($query))
        {
            $data+=array("status"=>"success","message"=>"Updated");
        }
        else
        {
            $data+=array("status"=>"failure","message"=>"Not Updated");
        }
        return response(json_encode($data));
    
    }
	

	



}
?>