<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    public $table='details';
    protected $fillable=['name','phone'];
}
